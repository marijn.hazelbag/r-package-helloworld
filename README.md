# R package helloworld

### step 1)  set working directory to the folder containing the package

# e.g. by using: setwd("C:/yourlocation")

### step 2) install the package using the code below:

# install.packages("helloworld", repos = NULL, type="source")

### step 3) run the package by running: helloworld()

# NB! running: helloworld (i..e without the ()) will print the function and not use the function 